# boca-db

BOCA Online Contest Administrator (known simply as BOCA) is an administration system to held programming contests (e.g., ACM-ICPC, Maratona de Programação da SBC). It uses PHP and PostgreSQL in the backend and its main features are portability, concurrency control, multi-site and distributed contests, and a simple web interface (for more details refer to https://www.ime.usp.br/~cassio/boca/).

The boca-db project provides a dockerized version of BOCA with a database client installed (e.g., MySQL, PostgreSQL etc.), aiming at easying the deployment of such a system for contests involving database programming skills.

## REQUIREMENTS:
- Install Docker Desktop (https://www.docker.com/get-started);

## GET STARTED:

* Open a Terminal window and prepare the environment.

        # List downloaded images
        docker images -a

        # List existing containers
        docker container ls -a

## HOW TO USE THIS DOCKER IMAGE:

1. Open a Terminal window and log in into GitLab’s Container Registry using your GitLab username and password.

        docker login registry.gitlab.com

1. Once you logged in, you’re able to pull the container image. Consider specifying the desired database client using the tag option (e.g., mysql). To see all database clients available check the project's Container Registry.

        docker pull registry.gitlab.com/rlaiola/boca-db:mysql

1. Start BOCA container using the following command:

        docker run -d -it -p 80:80 --privileged --name mybocadb registry.gitlab.com/rlaiola/boca-db:mysql

1. Open the web browser and visit http://localhost/boca. First, create and activate a BOCA contest (user: system | password: boca). Then, login as admin  (user: admin | password: boca) to manage users, problems, languages etc. NOTE: consider changing these passwords later on.

1. After enabling autocorrection (autojudge option on the Site tab), it will work only after you restart the container.

        docker stop mybocadb
        docker start mybocadb

## LICENSE:

Copyright (C) 2019-2020 Rodrigo Laiola Guimarães

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

## MORE INFORMATION:

For more detailed documentation see https://www.ime.usp.br/~cassio/boca/

Please report any issues with boca-db to https://gitlab.com/rlaiola/boca-db/-/issues
